import discord
from threading import Thread
import asyncio
from discord.ext.commands import Bot
from discord.ext import commands
import platform
import numpy
from ai import bestMove, checkWin
from flask import Flask, render_template, request
from functools import partial

BOARD_SIZE_Y = 6
BOARD_SIZE_X = 7
COMPUTER_PLAYER = 2
HUMAN_PLAYER = 1
app = Flask(__name__)

"""app.wsgi_app = SassMiddleware(app.wsgi_app, {
    'main': ('', '', '')
})"""

partial_run = partial(app.run, host="0.0.0.0", port=90, debug=True, use_reloader=False)

# Run the Flask app in another thread.
# Unfortunately this means we can't have hot reload
# (We turned it off above)
# Because there's no signal support.

t = Thread(target=partial_run)
t.start()

client = Bot(description = "BasicBot", command_prefix = "!")
games={}

class Stats:
    victory = 0
    lost = 0
    game_played = 0
    current_game = 0

    def end_game(self, wl): #wl = win or lose, 1 = win, 0 = lose
        if wl == 1:
            self.victory+=1
        else:
            self.lost+=1
    
    def new_game(self):
        self.game_played+=1

    def get(self):
        return "Stats:\nNumber of wins: " + str(self.victory) \
        + "\nNumber of lose: " + str(self.lost) \
        + "\nTotal games played: " + str(self.game_played) \
        + "\nCurrent games: " + str(len(games))

BotStats = Stats()

def check(board, x, y, player):
    check = 0
    if len(board) >= x + 4:
        for i in range(4):
            if board[x + i][y] != player:
                break
            elif i == 3:
                return player
    if len(board[x]) >= y + 4:
        for i in range(4):
            if board[x][y + i] != player:
                break
            elif i == 3:
                return player
    if len(board) >= x + 4 and len(board[x]) >= y + 4:
        for i in range(4):
            if board[x + i][y + i] != player:
                break
            elif i == 3:
                return player
    return 0

def victory(board):
    for x in range(len(board)):
        for y in range(len(board[x])):
            if (board[x][y] != 0):
                i = check(board, x, y, board[x][y])
                if i != 0:
                    return i
    return 0

def print_map(map):
    res=""
    for y in range(0,6):
        for x in range(0,7):
            res += ":white_circle:" if map[y][x] == 0 else ":red_circle:" if map[y][x] == 1 else ":yellow_circle:"
        res+="\n"
    res+=":one::two::three::four::five::six::seven:"
    return res

def destroy_board(id):
    del games[id]

def botTurn(board):
    move = bestMove(board, COMPUTER_PLAYER, HUMAN_PLAYER)
    return playAt(move+1, board, COMPUTER_PLAYER)

def userTurn(position, board):
    return playAt(position, board, HUMAN_PLAYER)

def new_move(user, position, board): # -1 == column already filled, -2 == draw, party is over, 1 == win user, 2 == win bot
    if not userTurn(position, board):
        return -1
    #if victory(board) == 1:
    if checkWin(board) == HUMAN_PLAYER:
        return 1
    botTurn(board)
    #if victory(board) == 2:
    if checkWin(board) == COMPUTER_PLAYER:
        return 2

async def send(ctx, message):
    await ctx.send("<@" + str(ctx.message.author.id) + ">\n" + message)

@client.event
async def on_ready():
    await client.change_presence(activity = discord.Game("Coding"))
    print("=" * 40 + "\n")
    print("Logged in as: {}".format(client.user.name))
    print("Bot's clientID: {}\n".format(client.user.id))
    print("Connected to {} guilds".format(len(client.guilds)))
    print("Connected to {} users\n".format(sum(1 for user in client.get_all_members())))
    print("Running discord.py version {}".format(discord.__version__))
    print("Running Python version {}\n".format(platform.python_version()))
    print("Invite your bot: https://discordapp.com/oauth2/authorize?client_id={}&scope=bot&permissions=8\n".format(client.user.id))
    print("="  * 40)

# List players in game
@client.command()
async def ingame(ctx):
    res = "List of players in game :"
    for game in games:
        res += "\n<@"+str(game)+">"
    await send(ctx, res)

@client.command()
async def quit(ctx):
    if ctx.message.author.id in games:
        destroy_board(ctx.message.author.id)
        await send(ctx, "You left the game")
    else:
        await send(ctx, "You're not in any game")

def playAt(position, board, player):
    for i in range(5,-1, -1):
        if board[i][position-1] == 0:
            board[i][position-1] = player
            return True
    return False

@client.command()
async def newgame(ctx):
    if ctx.channel.name == 'connect-4':
        BotStats.new_game()
        await ctx.send("Hey <@"+str(ctx.message.author.id)+">!\nYou are starting a connect 4 game !" +
        "\nType **!play** and a number **between 1 and 7** to begin the game.")
        games[ctx.message.author.id] = [[0 for col in range(BOARD_SIZE_X)] for row in range(BOARD_SIZE_Y)]

@client.command()
async def play(ctx, message=""):
    if ctx.channel.name == 'connect-4':
        id = ctx.message.author.id
        if not id in games:
            await send(ctx, "No game started.\nType **!newgame** to start one.")
            return
        if message == "" or not message.isnumeric() or (int(message) > 7 or int(message) < 1):
            await send(ctx ,"Please enter a **valid** number.\n**!play {1-7}**")
            return
        column = int(message)
        move_result = new_move(id, column, games[id])
        if move_result == -1:
            await send(ctx, "Couldn't put the number in this column. Try an other one.\n**!play {1-7}**")
            return
        elif move_result == 1:
            await send(ctx, print_map(games[id]))
            destroy_board(id)
            BotStats.end_game(0)
            await send(ctx, "You won !")
        elif move_result == 2:
            await send(ctx, print_map(games[id]))
            destroy_board(id)
            BotStats.end_game(1)
            await send(ctx, "Haha, you LOST !")
        else:
            await send(ctx, print_map(games[id]))

@client.command()
async def stats(ctx):
    await ctx.send(BotStats.get())

@app.route("/stats")
def stat():
    return str(BotStats);

@app.route("/")
def index():
    return render_template("index.html", wins=BotStats.victory, lose=BotStats.lost, games=BotStats.game_played)

client.run("Njk5OTgyNTU4Njk5NTg1NTQ2.XpcUBQ.pYS3FRPwg9FI7Fy73R8-tA7wXps")