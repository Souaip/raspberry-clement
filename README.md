<div align="center">

<img src="Docs/Images/DiscordBanner.png" height="190"/>

<p align="center">

19129029 **Clément ROYER** | **TOUSSAINT Steven** 19129122
</p>

<!-- omit in toc -->
# Connect-4 AI and game board on Discord

<!-- omit in toc -->
#### Assignment 1 | Embeded Systems

This project have been developed under instruction of the *Embeded systems* assignment n°1.

> Please contact us if you have any question or want a live demo. \
> clement.royer@epitech.eu || steven.toussaint@epitech.eu
<!-- omit in toc -->
##
</div>

1. [Project description](#project-description)
2. [How to use](#how-to-use)
3. [Proof of concept](#proof-of-concept)
4. [Installation](#installation)
   1. [Requirements](#requirements)
   2. [Local deployment](#local-deployment)
   3. [Deployment as server](#deployment-as-server)

## Project description

This project is a **Connect-4 AI bot** that uses *Discord* to compete against real human.

**The main goal of this project** is to **host an AI** and a game board, **connect it to *Discord*** to train or test it against real human and have dashboard to **see some stats about the AI performance**.

**Its main challenge have been** to create an **AI that can perform with the resources given by an emulated raspberry-pi**, **host** at a same time a **web server** and **connect the AI to discord**.

## How to use

After deploying the project, go to `localhost:90/`. \
If you already own a *Discord* server you can invite the bot to it, or join an existing server with the bot already deployed to it.

<div align="center">

  </h4>Here are the commands to use on <i>Discord</i></h4>

| Command | Explanation |
|-|-|
| `!help` | See the commands |
| `!newgame` | Start a new game |
| `!play y` | Place a token on the column y (Y is a number between 1 and 7) |
| `!stats` | See the stats |
| `!ingame` | See the players in a game |
| `!quit` | Quit your game |

</div>

## Proof of concept

With the discord bot, we are currently able to begin a new game and play against an artificial intelligence.

The AI works with a *MinMax* algorithm which allows it to have a chance to win. We put the depth of the *MinMax* to 4 to get a good balance between processing and fluency. With a Depth of 4 the AI is able to win almost every game played.

On the web server we can see different stats like the number of win, lose and the number of games being played since it has been deployed.

<div align="center">

  <h3>Bot in action</h3>
  <img src="Docs/Images/InAction.png" height="400"/>

</div>

## Installation

### Requirements

> You may need to install them as `sudo su -`

- `sudo apt-get install nginx`
- `sudo pip3 install discord`
- `sudo pip3 install numpy`
- `sudo apt-get install python3-flask`
- `pip3 install flask uwsgi`

### Local deployment

- `git clone https://gitlab.com/Souaip/raspberry-clement.git`
- `cd raspberry-clement`
- `python3 basicbot.py`
- Open your browser at 'localhost:90/'

### Deployment as server

> This deployment allows the server to start at the start of the rasberry-pi, and auto refresh when you modify the *[basicbot.py]()*.

- `sudo su -`
- `git clone https://gitlab.com/Souaip/raspberry-clement.git`
- `sudo chown www-data ./rasberry-clement`
- `cd rasberry-clement`
- `sudo nano uwsgi.ini`
- Copy/Past this text
  > Dont forget to modify it !
  ```
  [uwsgi]

  touch-reload = {Full path to dir}/raspberry-clement/basicbot.py
  chdir = {Full path to dir}/raspberry-clement
  module = basicbot:app

  master = true
  processes = 1
  threads = 2
  enable-threads = true

  uid = www-data
  gid = www-data

  socket = /tmp/basicbot.sock
  chmod-socket = 664
  vacuum = true
  die-on-term = true
  ```
- `sudo rm /etc/nginx/sites-enabled/default`
- `sudo nano /etc/nginx/sites-available/discordbot_proxy`
  ```
  server {
    listen 80;
    server_name localhost;

    location / { try_files $uri @app; }
    location @app {
      include uwsgi_params;
      uwsgi_pass unix:/tmp/discordbot.sock;
    }
  }
  ```
- `sudo ln -s /etc/nginx/sites-available/discordbot_proxy /etc/nginx/sites-enabled`
- `sudo systemctl restart nginx`
- `cd /etc/systemd/system`
- `sudo nano uwsgi.service`
  ```
  [Unit]
  Description=uWSGI Service
  After=network.target

  [Service]
  User=www-data
  Group=www-data
  WorkingDirectory=/home/pi/Desktop/raspberry-clement/
  ExecStart=/usr/local/bin/uwsgi --socket 0.0.0.0:8000 --protocol=http -w /home/pi/Desktop/raspberry-clement/basicbot:app --enable-threads

  [Install]
  WantedBy=multi-user.target
  ```
- `sudo systemctl daemon-reload`
- `sudo systemctl start uwsgi.service`
- `sudo systemctl status uwsgi.service`